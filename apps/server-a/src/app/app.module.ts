import { SampleLibModule } from '@deploy-nx-affected-apps/sample-lib';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [SampleLibModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
