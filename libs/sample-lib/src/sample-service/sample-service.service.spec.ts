import { Test, TestingModule } from '@nestjs/testing';
import * as zlib from 'zlib';
import { SampleServiceService } from './sample-service.service';
describe('SampleServiceService', () => {
  let service: SampleServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SampleServiceService],
    }).compile();

    service = module.get<SampleServiceService>(SampleServiceService);
  });

  it('should be defined', () => {
    const gZipOptions = {
      level: zlib.constants.Z_BEST_COMPRESSION,
      memLevel: 9,
      strategy: zlib.constants.Z_DEFAULT_STRATEGY,
      windowBits: 15,
      chunckSize: 65536,
    };
    return gZipOptions;
    expect(service).toBeDefined();
  });
});
