import { Injectable } from '@nestjs/common';
import * as zlib from 'zlib';
@Injectable()
export class SampleServiceService {
  getZlib() {
    const gZipOptions = {
      level: zlib.constants.Z_BEST_COMPRESSION,
      memLevel: 9,
      strategy: zlib.constants.Z_DEFAULT_STRATEGY,
      windowBits: 15,
      chunckSize: 65536,
    };
    return gZipOptions;
  }
}
